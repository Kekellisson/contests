#include <iostream>
#include <stdlib.h>

using namespace std;

int main(){
    char operation;
    cin >> operation;
    int size = 12;
    double matrix[size][size];

    for(int i = 0; i < size; i++){
        for(int j = 0; j < size; j++){
            cin >> matrix[i][j];
        }
    }

    double result = 0.0;
    double average = 30.0;
    int yend = 5;
    int i = 1;
    int j = 11;


    for(int y = 0; y < yend; y++){
        for(int ii = i; ii < j; ii++){
            result += matrix[y][ii];
        }
        i++;
        j--;
    }

    if(operation == 'M'){
        result = result / average;
    }

    cout.precision(1);
    cout << fixed << result << endl;
}
