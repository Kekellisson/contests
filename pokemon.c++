#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int HP(int lvl, int base, int iv, int ev){
    float result = iv + base + 50;
    result += sqrt(ev)/8.0;
    result *= lvl;
    result =  (int) result / 50;
    return (int) result + 10;
}

int baseStatus(int lvl, int base, int iv, int ev){
    float result = iv + base;
    result += sqrt(ev)/8.0;
    result *= lvl;
    result = (int) result / 50;
    return (int) result + 5;
}

int main(){

    int times;
    cin >> times;

    vector<vector <int> > attributes;
    attributes.resize(4);
    for(int i = 0; i < 4; i++){
        attributes[i].resize(3);
    }

    for(int i = 0; i < times; i++){
        string pokemon;
        int lvl;
        cin >> pokemon >> lvl;
        for(int j = 0; j < 4; j++){
            cin >> attributes[j][0] >> attributes[j][1]
                >> attributes[j][2];
        }
        cout << "Caso #" << i+1 << ": " << pokemon << " nivel " << lvl << endl;
        cout << "HP: "<< HP(lvl,attributes[0][0],attributes[0][1],attributes[0][2])<< endl;
        cout << "AT: "<< baseStatus(lvl,attributes[1][0],attributes[1][1],attributes[1][2])<< endl;
        cout << "DF: "<< baseStatus(lvl,attributes[2][0],attributes[2][1],attributes[2][2])<< endl;
        cout << "SP: "<< baseStatus(lvl,attributes[3][0],attributes[3][1],attributes[3][2])<< endl;

    }
    return 0;
}
