#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

void insert(vector<int> &predio, int andar){
    if((predio[abs(andar)] == -1 && andar > 0) || (predio[abs(andar)] == 1 && andar < 0)){
        predio[abs(andar)] = 2;
    }else{
        predio[abs(andar)] = andar/abs(andar);
    }
}

int andares(vector<int> predio, int floors){
    int sinal = 0; int size = 0; int max = 0;
    for(int i = 0; i < predio.size(); i++){
        if(max == floors){
            break;
        }
        if(predio[i] == 0){
            continue;
        }
        if(predio[i] == 2){
            size++; max++;
            sinal = - sinal;
        }else{
            if(predio[i] == -1 && sinal >= 0){
                size++; sinal = -1; max++;
            }else{
                if(predio[i] == 1 && sinal <= 0){
                    size++; sinal = 1; max++;
                }
            }
        }
    }
    return size;
}

int main(){
    int cases;
    cin >> cases;
    for(int i = 0; i < cases; i++){
        int floors;
        cin >> floors;
        vector<int> predio (500000,0);
        for(int j = 0; j < floors; j++){
            int andar;
            cin >> andar;
            insert(predio, andar);
        }
        cout << andares(predio, floors)<< endl;
    }
    return 0;
}
