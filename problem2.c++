#include <iostream>

using namespace std;

int podetobe(int a, int b, int c){

      if(a + b > c && a + c > b && b + c > a){
            //cout << a << " " << b << " " << c << "\n";
            return 1;
      }else{
            return 0;
      }
}


int main(){
      int matrix[4];

      cin >> matrix[0] >> matrix[1] >> matrix[2] >> matrix[3];

      int result = 0;

      result += podetobe(matrix[0], matrix[1], matrix[2]);
      result += podetobe(matrix[0], matrix[1], matrix[3]);
      result += podetobe(matrix[0], matrix[2], matrix[3]);
      result += podetobe(matrix[1], matrix[2], matrix[3]);

      if(result > 0)
            cout << 'S';
      else
            cout << 'N';

      return 0;
}
