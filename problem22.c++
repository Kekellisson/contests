#include <iostream>
#include <string>
//Guessing Game
using namespace std;

int main(){
    int guess;
    string too, what;

    cin >> guess;

    while(guess != 0){

        string liar = "Stan may be honest";
        cin >> too >> what;

        if(what == "on"){
            cout << liar << endl;
        }else{
        	int superior = 999999;
        	int inferior = -99999;
        	char end = 'n';

            if(what == "high"){
                superior = guess;
            }

            if(what == "low"){
                inferior = guess;
            }

        	do{

        		cin >> guess;
        		cin >> too >> what;

                if(guess > superior || (guess == superior && what != "high")){
        			liar = "Stan is dishonest";
        		}

        		if(guess < inferior || (guess == inferior && what != "low")){
        			liar = "Stan is dishonest";
        		}

        		if(what == "high"){
        			superior = guess;
        		}

        		if(what == "low"){
        			inferior = guess;
        		}

                if(what == "on"){
                    cout << liar << endl;
                    end = 'y';
                }

        	}while(end != 'y');

        }
        cin >> guess;
    }
    return 0;
}
