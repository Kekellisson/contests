#include <iostream>
#include <string>

using namespace std;

int main(){

    string cpf;

    while(getline(cin, cpf)){

        int b1 = 0; int b2 = 0; int index = 1;
        for(int i = 0; i < 11; i++){
            if(cpf[i] != '-' && cpf[i] != '.'){
                b2 += (cpf[i] - '0')*(10 - index);
                b1 += (cpf[i] - '0')*index++;
            }
        }
        b1 = b1 % 11;
        b2 = b2 % 11;
        if(b1 == 10) b1 = 0;
        if(b2 == 10) b2 = 0;
        if((cpf[12] - '0') == b1 && (cpf[13]  - '0') == b2){
            cout << "CPF valido" << endl;
        }else{
            cout << "CPF invalido" << endl;
        }
    }
}
