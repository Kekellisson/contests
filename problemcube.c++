#include <iostream>
#include <vector>
#include <string>

using namespace std;

void initCube(vector<vector<vector<int> > > &cube, vector<vector<int> >  &index, int n){
        //INSTANCIA CUBO E INICIALIZA OS VALORES
        cube.resize(n);
        index.resize(n);
        for(int i = 0; i < n; i++){
            cube[i].resize(n);
            index[i].resize(n);
            for(int j = 0; j < n; j++){
                cube[i][j].resize(n);
            }
        }

        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                index[i][j] = 0;
                for(int z = 0; z < n; z++){
                    cube[i][j][z] = 0;
                }
            }
        }
        ///////
}

void printCube(vector<vector<vector<int> > > cube, int n){
    cout << endl;
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            for(int z = 0; z < n; z++){
                cout << cube[i][j][z] << " ";
            }
            cout << endl;
        }
    }
    cout << endl;
}

bool VHP(vector<vector<vector<int> > > cube, int i, int j, int k, int n){
    bool v = true;
    bool h = true;
    bool p = true;
    int color = cube[i][j][k];
    for(int x = 0; x < n; x++){
        if((v == true) && (color != cube[i][j][x])){
            v = false;
        }
        if((h == true) && (color != cube[i][x][k])){
            h = false;
        }
        if((p == true) && (color != cube[x][j][k])){
            p = false;
        }
    }
    if(v == true || h == true || p == true){
        return true;
    }
    //cout << "NOT ME" << endl;
    return false;
}

bool diagonals(vector<vector<vector<int> > > cube, int i, int j, int k, int n){
    bool found;
    //cout << endl << i << " " << j << " " << k << endl;
    bool enableIK = false; bool enableKI = false; bool  enableKJ = false;
    bool enableJK = false; bool  enableIJ = false; bool  enableJI = false;
    if(i == k) enableIK = true; if(i == (n-1-k) || k == (n-1-i)) enableKI = true;
    if(k == j) enableKJ = true; if(k == (n-1-j) || j == (n-1-k)) enableJK = true;
    if(i == j) enableIJ = true; if(i == (n-1-j) || j == (n-1-i)) enableJI = true;

    //cout << enableIK << enableKI << enableKJ << enableJK << enableIJ << enableJI <<endl;

    int color = cube[i][j][k];
    //cout << "cor " << color << endl;
    for(int x = 0; x < n; x++){
        if(enableIK == true && color != cube[x][j][x]){
            enableIK = false;
        }
        if(enableKI == true && color != cube[n-1-x][j][x]){
            enableKI = false;
        }
        if(enableKJ == true && color != cube[i][x][x]){
            enableKJ = false;
        }
        if(enableJK == true && color != cube[i][n-1-x][x]){
            enableJK = false;
        }
        if(enableIJ == true && color != cube[x][x][k]){
            enableIJ = false;
        }
        if(enableJI == true && color != cube[n-1-x][x][k]){
            enableJI = false;
        }
    }

    //cout << enableIK << enableKI << enableKJ << enableJK << enableIJ << enableJI <<endl;

    if(enableIJ == true || enableIK == true || enableJI == true || enableKI == true || enableJK == true || enableKJ == true ){
        //cout << "TRT8" <<endl;
        return true;
    }
    return false;
}

bool internalDiags(vector<vector<vector<int> > > cube, int i, int j, int k, int n){
    int color = cube[i][j][k];
    bool found = false;
    if((i == j) && ((k == j)||(j == (n-1-k)))){
        found = true;
        int x = 0; int y = 0; int z = 0;
        while(x < n){
            if(color != cube[x][y][z]){
                found = false;
                break;
            }
            x++; y++; z++;
        }
        x = 0; y = 0; z = n-1;
        while(x < n){
            if(color != cube[x][y][z]){
                found = false;
                break;
            }
            x++; y++; z--;
        }
    }
    if(found == true){
        return true;
    }

    if(((n-1-j)== i) && ((k == j)||(j == (n-1-k)))){
        found = true;
        int x = n-1; int y = 0; int z = 0;
        while(y < n){
            if(color != cube[x][y][z]){
                found = false;
                break;
            }
            x--; y++; z++;
        }
        x = n-1; y = 0; z = n-1;
        while(y < n){
            if(color != cube[x][y][z]){
                found = false;
                break;
            }
            x--; y++; z--;
        }
    }
    return found;
}

bool match(vector<vector<vector<int> > > cube, int i, int j, int k, int n){
    bool found = VHP(cube, i, j, k, n);
    if(!found){
        found = diagonals(cube, i, j, k, n);
        if(!found){
            //cout << "MY FAULT!" << endl;
            found = internalDiags(cube, i, j, k, n);
        }
    }
    return found;
}

int main(){
    vector<vector<vector<int> > > cube;
    vector<vector<int> >  index;
    int n;
    cin >> n;
    int count = 1;
    while(n != 0){
        initCube(cube,index,n);
        bool found = false;
        int blue_or_white;
        for(int d = 0; d < n*n*n; d++){
            int i, j, k;
            cin >> i >> j;
            i--; j--;
            k = index[i][j];
            if(d % 2 == 0){
                cube[i][j][index[i][j]++] = 1;
            }else{
                cube[i][j][index[i][j]++] = -1;
            }
            if(found == false && d >= 2*n - 1){
                found = match(cube, i, j, k, n);
                if(found == true){
                    blue_or_white = d;
                    //cout << "AE CARAI" << endl;
                }
            }
        }
        string winner;
        if(found == true){
            if(blue_or_white % 2 == 0){
                winner = "Branco";
            }else{
                winner = "Azul";
            }
            cout << "Instancia " << count << endl << winner << " ganhou" << endl << endl;
        }else{
            cout << "Empate" << endl << endl;
        }
        count++;
        cin >> n;
    }


}
