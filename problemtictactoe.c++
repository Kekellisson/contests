#include <iostream>
#include <vector>
#include <string>

using namespace std;

void initCube(vector<vector<vector<int> > > &cube, vector<vector<int> >  &index, int n){
        //INSTANCIA CUBO E INICIALIZA OS VALORES
        cube.resize(n);
        index.resize(n);
        for(int i = 0; i < n; i++){
            cube[i].resize(n);
            index[i].resize(n);
            for(int j = 0; j < n; j++){
                cube[i][j].resize(n);
            }
        }

        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++){
                index[i][j] = 0;
                for(int z = 0; z < n; z++){
                    cube[i][j][z] = 0;
                }
            }
        }
        ///////
}

void initSlice(vector<vector<int> > &slice, int n){
    slice.resize(n);
    for(int i = 0; i < n; i++){
        slice[i].resize(n);
    }
}

void printSlice(vector<vector<int> > cube, int n){
    cout << endl;
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            cout << cube[i][j] << " ";
        }
        cout << endl;
    }
}


void printCube(vector<vector<vector<int> > > cube, int n){
    cout << endl;
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            for(int z = 0; z < n; z++){
                cout << cube[i][j][z] << " ";
            }
            cout << endl;
        }
    }
    cout << endl;
}

void sliceI(vector<vector<int> > &slice, vector<vector<vector<int> > > cube, int pivot, int n){
    for(int j = 0; j < n; j++){
        for(int z = 0; z < n; z++){
            slice[j][z] = cube[pivot][j][z];
        }
    }
}

void sliceJ(vector<vector<int> > &slice, vector<vector<vector<int> > > cube, int pivot, int n){
    for(int i = 0; i < n; i++){
        for(int z = 0; z < n; z++){
            slice[i][z] = cube[i][pivot][z];
        }
    }
}

void sliceZ(vector<vector<int> > &slice, vector<vector<vector<int> > > cube, int pivot, int n){
    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            slice[i][j] = cube[i][j][pivot];
        }
    }
}

bool horizontal(vector<vector<int> > slice, int n){
    for(int i = 0; i < n; i++){
        int cor = slice[i][0];
        bool flag = false;
        if(slice[i][n-1] == cor && cor != 0){
            flag = true;
            for(int j = 0; j < n; j++){
                if(slice[i][j] != cor){
                    flag = false;
                    break;
                }
            }
            if(flag == true){
                //cout << "AE CARAI" << endl;
                return true;
            }
        }
    }
    return false;
}

bool vertical(vector<vector<int> > slice, int n){
    for(int j = 0; j < n; j++){
        int cor = slice[0][j];
        bool flag = false;
        if(slice[n-1][j] == cor && cor != 0){
            flag = true;
            for(int i = 0; i < n; i++){
                if(slice[i][j] != cor){
                    flag = false;
                    break;
                }
            }
            if(flag == true){
                //cout << "AE CARAI" << endl;
                return true;
            }
        }
    }
    return false;
}

bool diags(vector<vector<int> > slice, int n){
    int cor = slice[0][0];
    if(cor != 0){
        int i = 1; int j = 1;
        bool found = true;
        while(i < n){
            if(cor != slice[i][j]){
                found = false;
                break;
            }
            i++; j++;
        }
        if(found == true){
            //cout << "DIAGS" << endl;
            return true;
        }
    }

    cor = slice[n-1][0];
    if(cor != 0){
        int i = n-2; int j = 1;
        bool found = true;
        while(j < n){
            if(cor != slice[i][j]){
                found = false;
                break;
            }
            i--; j++;
        }
        if(found == true){
            //cout << "DIAGS" << endl;
            return true;
        }
    }
    return false;
}

bool internalDiags(vector<vector<vector<int> > > cube, int n){
    int edges[12] = {0};
    edges[5] = n-1; edges[7] = n-1; edges[10] = n-1; edges[11] = n-1;
    for(int i = 0; i < 4; i++){
        int cor = cube[edges[3*i+0]][edges[3*i+1]][edges[3*i+2]];
        bool found = false;
        if(cor != 0){
            found = true;
            while(edges[3*i+0] < n){
                if(cor != cube[edges[3*i+0]][edges[3*i+1]][edges[3*i+2]]){
                    found = false;
                    break;
                }
                switch (i){
                    case 0: edges[3*i+0]++; edges[3*i+1]++; edges[3*i+2]++; break;
                    case 1: edges[3*i+0]++; edges[3*i+1]++; edges[3*i+2]--; break;
                    case 2: edges[3*i+0]++; edges[3*i+1]--; edges[3*i+2]++; break;
                    case 3: edges[3*i+0]++; edges[3*i+1]--; edges[3*i+2]--; break;

                }
            }
            if(found == true){
                //cout << "AE CARAIO"<< endl;
                return true;
            }
        }
    }
    return false;
}

bool searchAndDestroy(vector<vector<int> > slice, int n){
    bool found = false;
    found = vertical(slice, n);
    if(found == true){
        return true;
    }
    found = horizontal(slice, n);
    if(found == true){
        return true;
    }
    if(n > 1){
        found = diags(slice, n);
        if(found == true){
            return true;
        }
    }
    return false;
}

bool sliceEmAll(vector<vector<vector<int> > > &cube, int n){
    bool found = false;
    for(int i = 0; i < n; i++){
        vector<vector<int> > slice;
        initSlice(slice, n);
        sliceI(slice, cube, i, n);
        //printSlice(slice, n);
        found = searchAndDestroy(slice, n);
        if(found == true){
            //cout << "cARARI as" << endl;
            return true;
        }
    }
    for(int j = 0; j < n; j++){
        vector<vector<int> > slice;
        initSlice(slice, n);
        sliceJ(slice, cube, j, n);
        //printSlice(slice, n);
        found = searchAndDestroy(slice, n);
        if(found == true){
            //cout << "cARARI" << endl;
            return true;
        }
    }
    for(int z = 0; z < n; z++){
        vector<vector<int> > slice;
        initSlice(slice, n);
        sliceZ(slice, cube, z, n);
        //printSlice(slice, n);
        found = searchAndDestroy(slice, n);
        if(found == true){
            //cout << "cARARI ew" << endl;
            return true;
        }
    }
    if(found != true){
        return internalDiags(cube, n);
    }else{
        return true;
    }
}

int main(){
    vector<vector<vector<int> > > cube;
    vector<vector<int> >  index;
    int n;
    cin >> n;
    int count = 1;
    while(n != 0){
        initCube(cube,index,n);
        bool found = false;
        int blue_or_white;
        for(int d = 0; d < n*n*n; d++){
            int i, j;
            cin >> i >> j;
            i--; j--;
            if(d % 2 == 0){
                cube[i][j][index[i][j]++] = 1;
            }else{
                cube[i][j][index[i][j]++] = -1;
            }
            if(found == false){
                found = sliceEmAll(cube, n);
                if(found == true){
                    blue_or_white = d;
                }
            }
        }
        string winner;
        if(found == true){
            if(blue_or_white % 2 == 0){
                winner = "Branco";
            }else{
                winner = "Azul";
            }
            cout << "Instancia " << count << endl << winner << " ganhou" << endl << endl;
        }else{
            cout << "Empate" << endl << endl;
        }
        count++;
        cin >> n;
    }


}
